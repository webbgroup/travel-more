package entity

import "time"

type Package struct {
	ID          int
	CreatedAt   time.Time
	UserID      int
	Image       string `gorm:"type:varchar(255)"`
	Description string
}
type User struct {
	IDint    int
	FullName string `gorm:"type:varchar(255);not null"`
	Username string `gorm:"type:varchar(255);not null"`
	Email    string `gorm:"type:varchar(255);not null; unique"`
	Password string `gorm:"type:varchar(255)"`
	RoleID   uint
	Package  []Package
}
type Attraction struct {
	ID          int
	Name        string `gorm:"type:varchar(255);not null"`
	Description string
	Image       string `gorm:"type:varchar(255)"`
}

type Role struct {
	ID    uint
	Name  string `gorm:"type:varchar(255)"`
	Users []User
}
type Hotel struct {
	Id          int
	Name        string `gorm:"type:varchar(255);not null"`
	Description string
	Image       string `gorm:"type:varchar(255)"`
	Location    string `gorm:"type:varchar(255)"`
	Rating      string `gorm:"type:varchar(255)"`
}
type Session struct {
	ID         uint
	UUID       string `gorm:"type:varchar(255);not null"`
	Expires    int64  `gorm:"type:varchar(255);not null"`
	SigningKey []byte `gorm:"type:varchar(255);not null"`
}
type Error struct {
	Code    int
	Message string
}
