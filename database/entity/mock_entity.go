package entity

import "time"

var PackageMock = Package{
	ID:          1,
	CreatedAt:   time.Time{},
	UserID:      1,
	Image:       "mock_item.png",
	Description: "Mock Package 01 Description",
}

var RoleMock = Role{
	ID:    1,
	Name:  "Mock Role 01",
	Users: []User{},
}
var UserMock = User{
	IDint:    1,
	FullName: "Mock User 01",
	Username: "Mock user 01",
	Email:    "mockuser@example.com",
	RoleID:   1,
	Password: "P@$$w0rd",
	Package:  []Package{},
}
var AttractionMock = Attraction{
	ID:          1,
	Name:        "Mock Item 01",
	Description: "Mock Item 01 Description",
	Image:       "mock_item.png",
}
var HotelMock = Hotel{
	Id:          1,
	Name:        "Mock Item 01",
	Description: "Mock Item 01 Description",
	Image:       "Mock Item 01",
	Location:    "Mock Item 01 Location",
	Rating:      "Mock Item 01 Rating ",
}
var SessionMock = Session{
	ID:         1,
	UUID:       "_session_one",
	SigningKey: []byte("TravelGuide"),
	Expires:    0,
}
