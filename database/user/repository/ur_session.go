package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/_ibtissam_/web_project/database/entity"
	"gitlab.com/_ibtissam_/web_project/database/user"
)

type SessionGormRepo struct {
	conn *gorm.DB
}

func NewSessionGormRepo(db *gorm.DB) user.SessionRepository {
	return &SessionGormRepo{conn: db}

}
func (sr *SessionGormRepo) Session(sessionID string) (*entity.Session, []error) {
	session := entity.Session{}
	errs := sr.conn.Find(&session, "uuid=?", sessionID).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return &session, errs
}

func (sr *SessionGormRepo) StoreSession(session *entity.Session) (*entity.Session, []error) {
	sess := session
	errs := sr.conn.Save(sess).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return sess, errs
}

func (sr *SessionGormRepo) DeleteSession(sessionID string) (*entity.Session, []error) {
	sess, errs := sr.Session(sessionID)
	if len(errs) > 0 {
		return nil, errs
	}
	errs = sr.conn.Delete(sess, sess.ID).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return sess, errs
}
