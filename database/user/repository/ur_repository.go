package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/_ibtissam_/web_project/database/entity"
	"gitlab.com/_ibtissam_/web_project/database/user"
)

type UserGormRepo struct {
	conn *gorm.DB
}

func NewUserGormRepo(db *gorm.DB) user.UserRepository {
	return &UserGormRepo{conn: db}
}

func (userRepo *UserGormRepo) Users() ([]entity.User, []error) {
	user := []entity.User{}
	errs := userRepo.conn.Find(&user).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return user, errs
}
func (userRepo *UserGormRepo) UserByUsername(username string) (*entity.User, []error) {
	user := entity.User{}
	errs := userRepo.conn.Find(&user, "username=?", username).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return &user, errs
}

func (userRepo *UserGormRepo) User(id uint) (*entity.User, []error) {
	user := entity.User{}
	errs := userRepo.conn.First(&user, id).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return &user, errs
}

func (userRepo *UserGormRepo) UpdateUser(user *entity.User) (*entity.User, []error) {
	usr := user
	errs := userRepo.conn.Save(usr).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return usr, errs
}

func (userRepo *UserGormRepo) DeleteUser(id uint) (*entity.User, []error) {
	usr, errs := userRepo.User(id)
	if len(errs) > 0 {
		return nil, errs
	}
	errs = userRepo.conn.Delete(usr, id).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return usr, errs
}

// StoreUser stores a new user into the database
func (userRepo *UserGormRepo) StoreUser(users *entity.User) (*entity.User, []error) {
	usr := users
	errs := userRepo.conn.Create(usr).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return usr, errs
}
func (userRepo *UserGormRepo) UsernameExists(username string) bool {
	user := entity.User{}
	errs := userRepo.conn.Find(&user, "username=?", user).GetErrors()
	if len(errs) > 0 {
		return false
	}
	return true

}
func (userRepo *UserGormRepo) UserRoles(user *entity.User) ([]entity.Role, []error) {
	userRoles := []entity.Role{}
	errs := userRepo.conn.Model(user).Related(&userRoles).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return userRoles, errs
}
