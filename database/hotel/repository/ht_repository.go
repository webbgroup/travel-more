package repository

import (
	"gitlab.com/_ibtissam_/web_project/database/entity"
	"gitlab.com/_ibtissam_/web_project/database/hotel"
	"github.com/jinzhu/gorm"
)

type HotelGormRepo struct {
	conn *gorm.DB
}



func NewHotelGormRepo(db*gorm.DB) packge.HotelRepository {
	return &HotelGromRepo{conn: db}
}



func (hRepo *HotelGormRepo) Hotels() ([]entity.Hotel, []error){
	htl := []entity.Hotel{}
	errs := hRepo.conn.Find(&htl).GetErrors()
	if len(errs) >0{
		return nil, errs
	}
	return htl ,errs
}


func (hRepo *HotelGormRepo) Hotel(id uint) (*entity.Hotel, []error){
	htl :=entity.Hotel{}
	errs := hRepo.conn.First(&htl,id).GetErrors()
	if len(errs)>0{
		return nil, errs
	}
	return &htl, errs
}


func (hRepo *HotelGormRepo) UpdateHotel(hotel *entity.Hotel) (*entity.Hotel, []error) {
	htl := hotel
	errs := hRepo.conn.save(htl).GetErrors()
	if len(errs) >0{
		return nil, errs
	}
	return pac, errs
}func (hRepo *HotelGormRepo) DeleteHotel(id uint) (*entity.Hotel, []error) {
	htl, errs := hRepo.Hotel(id)
	if len(errs) > 0 {
		return nil, errs
	}
	errs = hRepo.conn.Delete(htl, htl.ID).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return htl ,errs



func (hRepo *HotelGormRepo) StoreHotel(hotel *entity.Hotel) (*entity.Hotel, []error) {
	htl :=hotel
	errs:=hRepo.conn.Create(htl).GetErrors()
	if len(errs) >0 {
		return nil,errs
	}
	return htl, errs
}
