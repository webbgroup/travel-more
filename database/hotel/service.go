package package

import "gitlab.com/_ibtissam_/web_project/database/entity"

/
type CategoryService interface {
	Hotels() ([]entity.Hotel, []error)
	Hotel(id uint) (*entity.Hotel, []error)
	UpdateHotel(hotel *entity.Hotel) (*entity., []error)
	DeleteHotel(id uint) (*entity.Hotel, []error)
	StoreHotel(hotel *entity.Hotel) (*entity.Hotel, []error)
	
}