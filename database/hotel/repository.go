package hotel

import "gitlab.com/_ibtissam_/web_project/database/entity"

// CategoryRepository specifies food menu category database operations
type HotelRepository interface {
	Hotels() ([]entity.Hotel, []error)
	Hotel(id uint) (*entity.Hotel, []error)
	UpdateHotel(hotel *entity.Hotel) (*entity.Hotel, []error)
	DeleteHotel(id uint) (*entity.Hotel, []error)
	StoreHotel(hotel *entity.Hotel) (*entity.Hotel, []error)
}
