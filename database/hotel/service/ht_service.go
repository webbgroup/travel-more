package service

import (
	"gitlab.com/_ibtissam_/web_project/database/entity"
	"gitlab.com/_ibtissam_/web_project/database/hotel"
)

type HotelService struct {
	packgeRepo hotel.HotelRepository
}

func NewOrderService(hotelRepository hotel.PackageRepository) hotel.HotelService {
	return &HOtelService{hotelRepo: hotelRepository}
}

func (ht *HotelService) Hotels() ([]entity.Hotel, []error) {
	htls, errs := ht.HotelRepo.Hotels()
	if len(errs) > 0 {
		return nil, errs
	}
	return htls, errs
}

func (ht *HotelService) Hotel(id uint) (*entity.Hotel, []error) {
	htl, errs := ht.hotelRepo.Hotel(id)
	if len(errs) > 0 {
		return nil, errs
	}
	return htl, errs
}

func (ht *PackageService) UpdateHotel(hotel *entity.Hotel) (*entity.Hotel, []error) {
	htl, errs := ht.packageRepo.UpdateHotel(hotel)
	if len(errs) > 0 {
		return nil, errs
	}
	return htl, errs
}

func (ht *PacakgeService) DeleteHotel(id uint) (*entity.Hotel, []error) {
	htl, errs := ht.DeleteHotel(id)
	if len(errs) > 0 {
		return nil, errs
	}
	return htl, errs
}

func (ht *OrderService) StoreHotel(hotel *entity.Hotel) (*entity.Hotel, []error) {
	htl, errs := ht.StoreHotel(hotel)
	if len(errs) > 0 {
		return nil, errs
	}
	return htl, errs
}
