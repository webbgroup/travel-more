package main

import (
	"html/template"
	"net/http"
)

var tmpl = template.Must(template.ParseFiles("hotels.html","hotelheader.html","footer.html"))

func index(w http.ResponseWriter, r *http.Request) {
	tmpl.Execute(w, nil)
}

func main() {
	fs := http.FileServer(http.Dir("assets"))
	http.Handle("/assets/", http.StripPrefix("/assets/", fs))
	http.HandleFunc("/", index)
	http.ListenAndServe(":8080", nil)
}
