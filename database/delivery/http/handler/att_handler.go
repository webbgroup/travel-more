package handler

import (
	"context"
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"gitlab.com/_ibtissam_/web_project/database/permission"
	"gitlab.com/_ibtissam_/web_project/database/rtoken"

	"gitlab.com/_ibtissam_/web_project/database/entity"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/_ibtissam_/web_project/database/form"
	"gitlab.com/_ibtissam_/web_project/database/user")

	type AttractionHandler struct {
		tmpl           *template.Template
		attractionSrv  attraction.AttractionService
		csrfSignKey    []byte
	}
	
	
	func NewAttractionHandler(T *template.Template, AT AttractionService, atKey []byte) *AttractionHandler {
		return &AttractionHandler{tmpl:T, attractionSrv: AT, csrfSignKey: atKey}
	}
	
	// too do
	func (at *AttractionHandler) AdminAttractions(w http.ResponsWriter, r *http.Request) {
		attract, errs := at.attractionSrv.Attractions()
		if errs != nil {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}
		token, err := rtoken.CSRFToken(at.csrfSignKey)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		tmplData := struct {
			Values     url.Values
			VErrors    form.ValidationErrors
			Attraction []entity.Attraction
			CSRF       string 
		}{
			Values:     nil,
			VErrors:    nil,
			Attractions: attractions,
			CSRF:       token,
		}
		at.tmpl.ExecuteTemplate(w, "admin.attraction.layout", tmplData)
	}
	//too do
	
	func (at *AttractionHandler) AdminAttractionsUpdate(w http.ResponseWriter, r *http.Request) {
		token. err :=rtoken.CSRFToken(at.csrfSignKey)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		if r.Method == http.MethodGet {
			idRaw := r.URL.Query().Get("id")
			id, err := strconv.Atoi(idRaw)
			if err != nil {
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			}
			attr, errs := at.attractionSrv.Attraction(uint(id))
			if len(errs) > 0 {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			values := url.Values{}
			values.Add("aid", idRaw)
			values.Add("aname", attr.Name)
			values.Add("adesc", attr.Description)
			values.Add("aimg", attr.Image)
			upAttForm := struct {
				Values   url.Values
				VErrors  form.ValidationErrors 
				Attraction *entity.Attraction
				CSRF     string
			}{
				Values:   values,
				VErrors:  form.ValidationErrors{},
				Attraction: attr,
				CSRF:     token,
			}
			at.tmpl.ExecuteTemplate(w, "admin.attraction.update.layout", upAttForm)
			return
			if r.Method == http.MethodPost {
				
				err := r.ParseForm()
				if err != nil {
					http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					return
				}
				
				updateAttorm := form.Input{Values: r.PostForm, VErrors: form.ValidationErrors{}}
				updateAttForm.Required("aname", "adesc",)
				updateAttForm.MinLength("adesc", 10)
				updateAttForm.CSRF = token
				
				if !updateAttForm.Valid() {
					at.tmpl.ExecuteTemplate(w, "admin.attraction.update.layout", updateAttForm)
					return
				}
				mf, fh, err := r.FormFile("aimg")
				if err != nil {
					updateAttForm.VErrors.Add("aimg", "File error")
					at.tmpl.ExecuteTemplate(w, "admin.attraction.update.layout", updayeAttForm)
					return
				}
				defer mf.Close()
				attr := &entity.Attraction{
					Name:        r.FormValue("aname"),
					Description: r.FormValue("adesc"),
					Image:       fh.Filename,
				}
				writeFile(&mf, fh.Filename)
				_, errs := at.attractionSrv.StoreAttraction(attr)
				if len(errs) > 0 {
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				http.Redirect(w, r, "/admin/attractions", http.StatusSeeOther)
			}
	}
	func (at *AttractionHandler) AdminAttractionsNew(w http.ResponsWRiter, r *http.Request) {
		
			token, err := rtoken.CSRFToken(at.csrfSignKey)
			if err != nil {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			if r.Method == http.MethodGet {
				newAttForm := struct {
					Values  url.Values
					VErrors form.ValidationErrors
					CSRF    string
				}{
					Values:  nil,
					VErrors: nil,
					CSRF:    token,
				}
				at.tmpl.ExecuteTemplate(w, "admin.attraction.new.layout", newAttForm)
			}
		
			if r.Method == http.MethodPost {
				
				err := r.ParseForm()
				if err != nil {
					http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					return
				}
				
				newAttorm := form.Input{Values: r.PostForm, VErrors: form.ValidationErrors{}}
				newAttForm.Required("aname", "adesc",)
				newAttForm.MinLength("adesc", 10)
				newAttForm.CSRF = token
				
				if !newAttForm.Valid() {
					at.tmpl.ExecuteTemplate(w, "admin.attraction.new.layout", newAttForm)
					return
				}
				mf, fh, err := r.FormFile("aimg")
				if err != nil {
					newAttForm.VErrors.Add("aimg", "File error")
					at.tmpl.ExecuteTemplate(w, "admin.attraction.new.layout", newAttForm)
					return
				}
				defer mf.Close()
				attr := &entity.Attraction{
					Name:        r.FormValue("aname"),
					Description: r.FormValue("adesc"),
					Image:       fh.Filename,
				}
				writeFile(&mf, fh.Filename)
				_, errs := at.attractionSrv.StoreAttraction(attr)
				if len(errs) > 0 {
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				http.Redirect(w, r, "/admin/attractions", http.StatusSeeOther)
			}
			func (at *AttractionHandler) AdminAttractionsDelete(w http.ResponseWriter, r *http.Request) {
				if r.Method == http.MethodGet {
					idRaw := r.URL.Query().Get("id")
					id, err := strconv.Atoi(idRaw)
					if err != nil {
						http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					}
					_, errs := at.attractionSrv.DeleteAttraction(uint(id))
					if len(errs) > 0 {
						http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
					}
				}
				http.Redirect(w, r, "/admin/attractions", http.StatusSeeOther)
			}
			
			func writeFile(mf *multipart.File, fname string) error {
				wd, err := os.Getwd()
				if err != nil {
					return err
				}
				path := filepath.Join(wd, "ui", "assets", "img", fname)
				image, err := os.Create(path)
				if err != nil {
					return err
				}
				defer image.Close()
				io.Copy(image, *mf)
				return nil
			}
			
	
		
	
	}
	
}  
		

	



 