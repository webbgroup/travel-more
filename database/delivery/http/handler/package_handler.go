package handler

import (
	"context"
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"gitlab.com/_ibtissam_/web_project/database/permission"
	"gitlab.com/_ibtissam_/web_project/database/rtoken"
	"gitlab.com/_ibtissam_/web_project/database/entity"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/_ibtissam_/web_project/database/form"
	"gitlab.com/_ibtissam_/web_project/database/user")

	type HotelHandler struct {
		tmpl           *template.Template
		packageSrv       package.PackageService
		csrfSignKey    []byte
	}
	
	
	func NewPackageHandler(T *template.Template, PK packageService, pkKey []byte) *PackageHandler {
		return &PackageHandler{tmpl:T, packageSrv: PK, csrfSignKey: pkKey}
	}
	
	// too do
	func (pk *PackageHandler) AdminPackges(w http.ResponsWriter, r *http.Request) {
		hot, errs := pk.packageSrv.Packagess()
		if errs != nil {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}
		token, err := rtoken.CSRFToken(pk.csrfSignKey)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		tmplData := struct {
			Values     url.Values
			VErrors    form.ValidationErrors
			Packages   []entity.Package
			CSRF       string
		}{
			Values:     nil,
			VErrors:    nil,
			Packages: packages,
			CSRF:       token,
		}
		pk.tmpl.ExecuteTemplate(w, "admin.Package.layout", tmplData)
	}
	//too do
	func (pk *HotelHandler) AdminPackageNew(w http.ResponsWRiter, r *http.Request) {
		
			token, err := rtoken.CSRFToken(pk.csrfSignKey)
			if err != nil {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			if r.Method == http.MethodGet {
				newPackageForm := struct {
					Values  url.Values
					VErrors form.ValidationErrors
					CSRF    string
				}{
					Values:  nil,
					VErrors: nil,
					CSRF:    token,
				}
				pk.tmpl.ExecuteTemplate(w, "admin.package.new.layout", newPackageForm)
			}
		
			if r.Method == http.MethodPost {
				
				err := r.ParseForm()
				if err != nil {
					http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					return
				}
				
				newPackageForm := form.Input{Values: r.PostForm, VErrors: form.ValidationErrors{}}
				newPackageForm.Required("pname", "pdesc")
				newPackageForm.MinLength("pdesc", 10)
				newPackageForm.CSRF = token
				
				if !newPackageForm.Valid() {
					pk.tmpl.ExecuteTemplate(w, "admin.package.new.layout", newPackageForm)
					return
				}
				mf, fh, err := r.FormFile("pkimg")
				if err != nil {
					newPackageForm.VErrors.Add("pkimg", "File error")
					pk.tmpl.ExecuteTemplate(w, "admin.package.new.layout", newPackageForm)
					return
				}
				defer mf.Close()
				pak := &entity.Package{
					Name:        r.FormValue("pname"),
					Description: r.FormValue("pdesc"),
					UUID:        *entity.User
					Image:       fh.Filename,
				}
				writeFile(&mf, fh.Filename)
				_, errs := pk.packageSrv.StorePackage(pak)
				if len(errs) > 0 {
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				http.Redirect(w, r, "/admin/package", http.StatusSeeOther)
			}
	func (pk *HotelHandler) AdminHotelsUpdate(w http.ResponseWriter, r *http.Request) {
				token. err :=rtoken.CSRFToken(pk.csrfSignKey)
				if err != nil {
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				if r.Method == http.MethodGet {
					idRaw := r.URL.Query().Get("id")
					id, err := strconv.Atoi(idRaw)
					if err != nil {
						http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					}
					pkg, errs := pk.attractionSrv.Attraction(uint(id))
					if len(errs) > 0 {
						http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
					}
					values := url.Values{}
					values.Add("pid", idRaw)
					values.Add("pname", pkg.Name)
					values.Add("pdesc", pkg.Description)
					values.Add("pimg", pkg.Image)
					upAttForm := struct {
						Values   url.Values
						VErrors  form.ValidationErrors 
						Package *entity.Package
						CSRF     string
					}{
						Values:   values,
						VErrors:  form.ValidationErrors{},
						Hotel:    pkg,
						CSRF:     token,
					}
					pk.tmpl.ExecuteTemplate(w, "admin.package.update.layout", upAttForm)
					return
					if r.Method == http.MethodPost {
						
						err := r.ParseForm()
						if err != nil {
							http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
							return
						}
						
						updateHotlForm := form.Input{Values: r.PostForm, VErrors: form.ValidationErrors{}}
						updateHotlForm.Required("pname", "pdesc",)
						updateHotlForm.MinLength("pdesc", 10)
						updateHotlForm.CSRF = token
						
						if !updateAttForm.Valid() {
							at.tmpl.ExecuteTemplate(w, "admin.package.update.layout", updateHotlForm)
							return
						}
						mf, fh, err := r.FormFile("pimg")
						if err != nil {
							updateAttForm.VErrors.Add("pimg", "File error")
							at.tmpl.ExecuteTemplate(w, "admin.package.update.layout", updateHotlForm)
							return
						}
						defer mf.Close()
						attr := &entity.Attraction{
							Name:        r.FormValue("pname"),
							Description: r.FormValue("pdesc"),
							Image:       fh.Filename,
						}
						writeFile(&mf, fh.Filename)
						_, errs := at.attractionSrv.StoreAttraction(attr)
						if len(errs) > 0 {
							http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
						}
						http.Redirect(w, r, "/admin/pakages", http.StatusSeeOther)
					}
			func (pk *PackageHandler) AdminPackagesDelete(w http.ResponseWriter, r *http.Request) {
				if r.Method == http.MethodGet {
					idRaw := r.URL.Query().Get("id")
					id, err := strconv.Atoi(idRaw)
					if err != nil {
						http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					}
					_, errs := ht.packageSrv.DeletePackage(uint(id))
					if len(errs) > 0 {
						http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
					}
				}
				http.Redirect(w, r, "/admin/packages", http.StatusSeeOther)
			}
			
			func writeFile(mf *multipart.File, fname string) error {
				wd, err := os.Getwd()
				if err != nil {
					return err
				}
				path := filepath.Join(wd, "ui", "assets", "img", fname)
				image, err := os.Create(path)
				if err != nil {
					return err
				}
				defer image.Close()
				io.Copy(image, *mf)
				return nil
			}
			
	
	
	
	}
	
}  