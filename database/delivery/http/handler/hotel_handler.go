package handler

import (
	"context"
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"gitlab.com/_ibtissam_/web_project/database/permission"
	"gitlab.com/_ibtissam_/web_project/database/rtoken"

	"gitlab.com/_ibtissam_/web_project/database/entity"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/_ibtissam_/web_project/database/form"
	"gitlab.com/_ibtissam_/web_project/database/user")

	type HotelHandler struct {
		tmpl           *template.Template
		hotelSrv       hotel.HotelService
		csrfSignKey    []byte
	}
	
	
	func NewHotelHandler(T *template.Template, HT HotelService, htKey []byte) *HotelHandler {
		return &HotelHandler{tmpl:T, hotelSrv: AT, csrfSignKey: htKey}
	}
	
	// too do
	func (ht *HotelHandler) AdminHotels(w http.ResponsWriter, r *http.Request) {
		attract, errs := ht.hotelSrv.Hotels()
		if errs != nil {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}
		token, err := rtoken.CSRFToken(pk.csrfSignKey)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		tmplData := struct {
			Values     url.Values
			VErrors    form.ValidationErrors
			Hotels     []entity.Hotel
			CSRF       string
		}{
			Values:     nil,
			VErrors:    nil,
			Hotels:     hotels,
			CSRF:       token,
		}
		ht.tmpl.ExecuteTemplate(w, "admin.hotel.layout", tmplData)
	}
	//too do
	func (ht *HotelHandler) AdminHotelNew(w http.ResponsWRiter, r *http.Request) {
		
			token, err := rtoken.CSRFToken(ht.csrfSignKey)
			if err != nil {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			if r.Method == http.MethodGet {
				newHotelForm := struct {
					Values  url.Values
					VErrors form.ValidationErrors
					CSRF    string
				}{
					Values:  nil,
					VErrors: nil,
					CSRF:    token,
				}
				ht.tmpl.ExecuteTemplate(w, "admin.hotel.new.layout", newHotelForm)
			}
		
			if r.Method == http.MethodPost {
				
				err := r.ParseForm()
				if err != nil {
					http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					return
				}
				
				newHotelForm := form.Input{Values: r.PostForm, VErrors: form.ValidationErrors{}}
				newHotelForm.Required("hname", "hdesc","hlocation","hrating")
				newHotelForm.MinLength("hdesc", 10)
				newHotelForm.MinLength("hlocation", 10)
				newHotelForm.MinLength("hrating", 1)
				newHotelForm.CSRF = token
				
				if !newHotelForm.Valid() {
					ht.tmpl.ExecuteTemplate(w, "admin.hotel.new.layout", newAttForm)
					return
				}
				mf, fh, err := r.FormFile("himg")
				if err != nil {
					newHotelForm.VErrors.Add("himg", "File error")
					ht.tmpl.ExecuteTemplate(w, "admin.hotel.new.layout", newAttForm)
					return
				}
				defer mf.Close()
				htt := &entity.Hotel{
					Name:        r.FormValue("hname"),
					Description: r.FormValue("hdesc"),
					Location:    r.FormValue("hlocation"),
					Rating:      r.FormValue("hrating"),
					Image:       fh.Filename,
				}
				writeFile(&mf, fh.Filename)
				_, errs := ht.hotelSrv.StoreHotel(htt)
				if len(errs) > 0 {
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				http.Redirect(w, r, "/admin/hotel", http.StatusSeeOther)
			}
			func (ht *HotelHandler) AdminHotelsUpdate(w http.ResponseWriter, r *http.Request) {
				token. err :=rtoken.CSRFToken(ht.csrfSignKey)
				if err != nil {
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				if r.Method == http.MethodGet {
					idRaw := r.URL.Query().Get("id")
					id, err := strconv.Atoi(idRaw)
					if err != nil {
						http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					}
					hotl, errs := ht.attractionSrv.Attraction(uint(id))
					if len(errs) > 0 {
						http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
					}
					values := url.Values{}
					values.Add("hid", idRaw)
					values.Add("hname", hotl.Name)
					values.Add("hdesc", hotl.Description)
					values.Add("hlocation", hotl.Location)
					values.Add("hrating", hotl.Rating)
					values.Add("himg", hotl.Image)
					upAttForm := struct {
						Values   url.Values
						VErrors  form.ValidationErrors 
						Hotel *entity.Hotel
						CSRF     string
					}{
						Values:   values,
						VErrors:  form.ValidationErrors{},
						Hotel:    hotl,
						CSRF:     token,
					}
					ht.tmpl.ExecuteTemplate(w, "admin.hotel.update.layout", upAttForm)
					return
					if r.Method == http.MethodPost {
						
						err := r.ParseForm()
						if err != nil {
							http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
							return
						}
						
						updateHotlForm := form.Input{Values: r.PostForm, VErrors: form.ValidationErrors{}}
						updateHotlForm.Required("hname", "hdesc",)
						updateHotlForm.MinLength("hdesc", 10)
						updateHotlForm.CSRF = token
						
						if !updateAttForm.Valid() {
							at.tmpl.ExecuteTemplate(w, "admin.hotel.update.layout", updateHotlForm)
							return
						}
						mf, fh, err := r.FormFile("himg")
						if err != nil {
							updateAttForm.VErrors.Add("himg", "File error")
							at.tmpl.ExecuteTemplate(w, "admin.hotel.update.layout", updateHotlForm)
							return
						}
						defer mf.Close()
						attr := &entity.Attraction{
							Name:        r.FormValue("hname"),
							Description: r.FormValue("hdesc"),
							Image:       fh.Filename,
						}
						writeFile(&mf, fh.Filename)
						_, errs := at.attractionSrv.StoreAttraction(attr)
						if len(errs) > 0 {
							http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
						}
						http.Redirect(w, r, "/admin/hotels", http.StatusSeeOther)
					}
			func (ht *HotelHandler) AdminHotelsDelete(w http.ResponseWriter, r *http.Request) {
				if r.Method == http.MethodGet {
					idRaw := r.URL.Query().Get("id")
					id, err := strconv.Atoi(idRaw)
					if err != nil {
						http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
					}
					_, errs := ht.hotelSrv.DeleteHotel(uint(id))
					if len(errs) > 0 {
						http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
					}
				}
				http.Redirect(w, r, "/admin/hotels", http.StatusSeeOther)
			}
			
			func writeFile(mf *multipart.File, fname string) error {
				wd, err := os.Getwd()
				if err != nil {
					return err
				}
				path := filepath.Join(wd, "ui", "assets", "img", fname)
				image, err := os.Create(path)
				if err != nil {
					return err
				}
				defer image.Close()
				io.Copy(image, *mf)
				return nil
			}
			
	
		
	
	}
	
}  