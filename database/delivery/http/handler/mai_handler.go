package handler

import (
	"html/template"
	"net/http"
	"net/url"

	"gitlab.com/_ibtissam_/web_project/database/form"

	"gitlab.com/_ibtissam_/web_project/database/rtoken"
)

type MainHandler struct {
	tmpl        *template.Template
	csrfSignKey []byte
}

func NewMainHandler(T *template.Template, csKey []byte) *MenuHandler {
	return &MenuHandler{tmpl: T, csrfSignKey: csKey}
}

func (mh *MenuHandler) Index(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	token, err := rtoken.CSRFToken(mh.csrfSignKey)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
	tmplData := struct {
		Values  url.Values
		VErrors form.ValidationErrors
		CSRF    string
	}{
		Values:  nil,
		VErrors: nil,
		CSRF:    token,
	}

	mh.tmpl.ExecuteTemplate(w, "home.layout", tmplData)
}

func (mh *MenuHandler) Contact(w http.ResponseWriter, r *http.Request) {
	token, err := rtoken.CSRFToken(mh.csrfSignKey)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
	tmplData := struct {
		Values  url.Values
		VErrors form.ValidationErrors
		CSRF    string
	}{
		Values:  nil,
		VErrors: nil,
		CSRF:    token,
	}
	mh.tmpl.ExecuteTemplate(w, "contact.layout", tmplData)
}

func (mh *MenuHandler) Admin(w http.ResponseWriter, r *http.Request) {
	token, err := rtoken.CSRFToken(mh.csrfSignKey)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
	tmplData := struct {
		Values  url.Values
		VErrors form.ValidationErrors
		CSRF    string
	}{
		Values:  nil,
		VErrors: nil,
		CSRF:    token,
	}
	mh.tmpl.ExecuteTemplate(w, "admin.index.layout", tmplData)
}
