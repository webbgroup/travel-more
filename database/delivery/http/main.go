package main

import (
	"html/template"
	"net/http"
	"time"

	"gitlab.com/_ibtissam_/web_project/database/delivery/http/handler"
	"gitlab.com/_ibtissam_/web_project/database/entity"
	prepim "gitlab.com/_ibtissam_/web_project/database/package/repository"
	psrvim "gitlab.com/_ibtissam_/web_project/database/package/service"
	"gitlab.com/_ibtissam_/web_project/database/rtoken"
    arepim "gitlab.com/_ibtissam_/web_project/database/attraction/repository"
	asrvim"gitlab.com/_ibtissam_/web_project/database/attraction/repository"
	hrepim"gitlab.com/_ibtissam_/web_project/database/hotel/repository"
    hsrvim "gitlab.com/_ibtissam_/web_project/database/hotel/repository"

	urepimp "gitlab.com/_ibtissam_/web_project/database/user/repository"
	usrvimp "gitlab.com/_ibtissam_/web_project/database/user/service"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func createTables(dbconn *gorm.DB) []error {
	errs := dbconn.CreateTable(&entity.User{}, &entity.Role{}, &entity.Session{}, &entity.Item{}, &entity.Order{}, &entity.Category{}, &entity.Ingredient{}, &entity.Comment{}).GetErrors()
	if errs != nil {
		return errs
	}
	return nil
}
func main() {
	//createTables(dbconn)

	csrfSignKey := []byte(rtoken.GenerateRandomID(32))
	tmpl := template.Must(template.ParseGlob("ui/templates/*"))

	dbconn, err := gorm.Open("postgres", "postgres://postgres:ibtissam@localhost/restaurantdb?sslmode=disable")

	if err != nil {
		panic(err)
	}

	defer dbconn.Close()

    sessioRepo := urepimp.NewSessionGormRepo(dbconn)
	sessSrv := usrvimp.NewSessionService(sessionRepo)


	packageRepo := prepim.NewPackageGormRepo(dbconn)
	packageServ := psrvim.NewPackageService(packageRepo)

	hotelRepo := hrepim.NewHotelGormRepo(dbconn)
	hotelserv := hsrvim.NewHotelService(hotelRepo)

	userRepo := urepimp.NewUserGormRepo(dbconn)
	userServ := usrvimp.NewUserService(userRepo)
  
	roleRepo := urepimp.NewRoleGormRepo(dbconn)
	roleServ := usrvimp.NewRoleService(roleRepo)
     
    attRepo := arepimp.NewAttractionGormRepo(dbconn)
	attSrv := asrvimp.NewAttractionService(attRepo)

	sess := configSess()
	uh := handler.NewUserHandler(tmpl, userServ, sessionSrv, roleServ, sess, csrfSignKey)

    attr := handler.NewAdminAttractionHandler(tmpl, attractionServ, csrfSignKey)
	mh := handler.MainHandler(tmpl, attractionServ, csrfSignKey)
    fs := http.FileServer(http.Dir("ui/assets"))
	http.Handle("/assets/", http.StripPrefix("/assets/", fs))
	http.HandleFunc("/", mh.home)
	http.HandleFunc("/contact", mh.Contact)
	http.HandleFunc("/admin", Authenticated(uh.Authorized(http.HandlerFunc(mh.Admin))))

	http.Handle("/admin/attractions", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminAttractions))))
	http.Handle("/admin/attractions/new", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminAttractionsNew))))
	http.Handle("/admin/attractions/update", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminAttractionsUpdate))))
	http.Handle("/admin/attractions/delete", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminAttractionsDelete))))

	http.Handle("/admin/hotels", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminHotels))))
	http.Handle("/admin/hotels/new", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminHotelsNew))))
	http.Handle("/admin/hotels/update", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminHotelsUpdate))))
	http.Handle("/admin/hotels/delete", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminHotelsDelete))))

    http.Handle("/admin/packages", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminPackages))))
	http.Handle("/admin/packages/new", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminPackagesNew))))
	http.Handle("/admin/packages/update", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminPackagesUpdate))))
	http.Handle("/admin/packages/delete", uh.Authenticated(uh.Authorized(http.HandlerFunc(ach.AdminPackagesDelete))))

	http.HandleFunc("/login", uh.Login)
	http.Handle("/logout", uh.Authenticated(http.HandlerFunc(uh.Logout)))
	http.HandleFunc("/signup", uh.Signup)

	http.ListenAndServe(":8181", nil)

}	
func configSess() *entity.Session {
	tokenExpires := time.Now().Add(time.Minute * 30).Unix()
	sessionID := rtoken.GenerateRandomID(32)
	signingString, err := rtoken.GenerateRandomString(32)
	if err != nil {
		panic(err)
	}
	signingKey := []byte(signingString)

	return &entity.Session{
		Expires:    tokenExpires,
		SigningKey: signingKey,
		UUID:       sessionID,
	}
}
