package service

import (
	"gitlab.com/_ibtissam_/web_project/database/entity"
	"gitlab.com/_ibtissam_/web_project/database/packageee"
)

type PackageService struct {
	packgeRepo package.PackageRepository
}


func NewOrderService( packageRepository package.PackageRepository) package.PackageService {
	return &PackageService{packageRepo: packageRepository}
}


func (pg *PackageService) Packages() ([]entity.Package, []error) {
	pkgs, errs := pg.packageRepo.Packages()
	if len(errs) > 0 {
		return nil, errs
	}
	return pkgs, errs
}


func (pg *PackageService) Package(id uint) (*entity.Package, []error) {
	pkg, errs := pg.packageRepo.Package(id)
	if len(errs) > 0 {
		return nil, errs
	}
	return pkg, errs
}


func (pg *PackageService) UserPackages(user *entity.User) ([]entity.Package, []error) {
	pkgs, errs := pg.packageRepo.UserPackages(user)
	if len(errs) > 0 {
		return nil, errs
	}
	return pkgs, errs
}


func (pg *PackageService) UpdatePackage(packagee *entity.Package) (*entity.Package, []error) {
	pkg, errs := pg.packageRepo.UpdatePackage(packagee)
	if len(errs) > 0 {
		return nil, errs
	}
	return pkg, errs
}


func (pg *PacakgeService) DeletePackage(id uint) (*entity.Package, []error) {
	pkg, errs := pg.DeletePackage(id)
	if len(errs) > 0 {
		return nil, errs
	}
	return pkg, errs
}


func (pg *PackageService) StorePackage(packagee *entity.Package) (*entity.Package, []error) {
	pkg, errs := pg.StorePackage(packagee)
	if len(errs) > 0 {
		return nil, errs
	}
	return pkg, errs
}
