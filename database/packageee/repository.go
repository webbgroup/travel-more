package packageee

import "gitlab.com/_ibtissam_/web_project/database/entity"

// CategoryRepository specifies food menu category database operations
type PackageRepository interface {
	Packages() ([]entity.Package, []error)
	Package(id uint) (*entity.Package, []error)
	UpdatePackage(packagee *entity.Package) (*entity.Package, []error)
	DeletePackage(id uint) (*entity.Package, []error)
	StorePackage(packagee *entity.Package) (*entity.Package, []error)
}
