package packageee

import "gitlab.com/_ibtissam_/web_project/database/entity"

type CategoryService interface {
	Categories() ([]entity.Package, []error)
	Category(id uint) (*entity.Package, []error)
	UpdateCategory(packagee *entity.Package) (*entity.Package, []error)
	DeleteCategory(id uint) (*entity.Package, []error)
	StoreCategory(packagee *entity.Package) (*entity.Package, []error)
}
