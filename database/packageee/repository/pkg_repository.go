package repository

import (
	"gitlab.com/_ibtissam_/web_project/database/entity"
	"gitlab.com/_ibtissam_/web_project/database/packageee"
	"github.com/jinzhu/gorm"
)

type PackageGormRepo struct {
	conn *gorm.DB
}



func NewPackageGormRepo(db*gorm.DB) packge.PackageRepository {
	return &PackageGromRepo{conn: db}
}



func (pRepo *PackageGormRepo) Packages() ([]entity.Package, []error){
	pkgs := []entity.Package{}
	errs := pRepo.conn.Find(&pkgs).GetErrors()
	if len(errs) >0{
		return nil, errs
	}
	return pkgs ,errs
}


func (pRepo *PackageGormRepo) Package(id uint) (*entity.Package, []error){
	pkg :=entity.Package{}
	errs := pRepo.conn.First(&pkg,id).GetErrors()
	if len(errs)>0{
		return nil, errs
	}
	return &pkg, errs
}


func (pRepo *PackageGormRepo) UpdatePackage(packagee *entity.Package) (*entity.Package, []error) {
	pac :=packagee
	errs := cRepo.conn.save(pac).GetErrors()
	if len(errs) >0{
		return nil, errs
	}
	return pac, errs
}
func (pRepo *PackageGormRepo) DeletePackage(id uint) (*entity.Package, []error) {
	pac, errs := cRepo.Package(id)
	if len(errs) > 0 {
		return nil, errs
	}
	errs = pRepo.conn.Delete(pac, pac.ID).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return pac ,errs



func (pRepo *PackageGormRepo) StorePackage(packagee *entity.Package) (*entity.Package, []error) {
	pac :=packagee
	errs :=pRepo.conn.Create(pac).GetErrors()
	if len(errs) >0 {
		return nil,errs
	}
	return pac, errs
}

func (pRepo *PackageGormRepo) UserPackage(user *entity.User)  ([]entity.Package, []error){
	uspac := []entity.Package{}
	errs:=pRepo.conn.Model(user).Related(&uspac, "Packages").GetErrors()
	if len(errs) >0 {
		return nil ,errs
	}
	return uspac, errs
}