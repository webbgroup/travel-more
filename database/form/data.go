package form

import (
	"fmt"
	"net/url"
	"regexp"
	"unicode/utf8"
)

var UsernameRX = regexp.MustCompile("^[a-zA-Z]$")

type Input struct {
	Values url.Values
	VErrors ValidationErrors
	CSRF    string
}

func (inVal *Input) MinLength(field string, d int)  {
	value :=inVal.Values.Get(field)
	if value == ""{
		return
	}
	if utf8.RuneCountInString(value) < d {
		inVal.VErrors.Add(field, fmt.Sprintf("This field is too short (minimum is %d caharacters)",d))

	}
	
}

func (inVal *Input) Required(field ...string)  {
	for _,f :=range field {
		value := inVal.Values.Get(f)
		if value == "" {
			inVal.VErrors.Add(f, "This field is required field")
		}
	}
}
func (inVal *Input) MatchesPattern(field string, pattern *regexp.Regexp) {
	value := inVal.Values.Get(field)
	if value == "" {
		return
	}
	if !pattern.MatchString(value) {
		inVal.VErrors.Add(field, "The value entered is invalid")
	}
}

func (inVal *Input) PassewordMatches(password string, confpassword string) {
	pwd := inVal.Values.Get(password)
	confpwd := inVal.Values.Get(confpassword)

	if pwd == "" || confpwd == "" {
		return
	}
	if pwd != confpwd {
		inVal.VErrors.Add(password, "The password and confim password values did not match")
		inVal.VErrors.Add(confpassword,"The password and confim password value did nt match")
		
	}
}

func (inVal *Input) Valid() bool  {
	return len(inVal.VErrors) == 0
	
}


