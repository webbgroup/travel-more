package form


type ValidationErrors map[string][]string


func (ve ValidationErrors) Add(field, message string) {
	ve[field] = append(ve[field], message)
}


func (ve ValidationErrors) Get(field string) string {
	ves := ve[field]
	if len(ves) == 0 {
		return ""
	}
	return ves[0]
}