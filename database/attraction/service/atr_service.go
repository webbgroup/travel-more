package repository

import (
	"gitlab.com/_ibtissam_/web_project/database/attraction"
	"gitlab.com/_ibtissam_/web_project/database/entity"
)

type AttractionService struct {
	attractionRepo attraction.AttractionRepository
}

func (att *AttractionService) Attractions() ([]entity.Attraction, []error) {
	attrs, errs := att.attractionRepo.Attractions()

	if len(errs) > 0 {
		return nil, errs
	}
	return attrs, nil
}
func (att *AttractionService) StoreAttraction(attract *entity.Attraction) (*entity.Attraction, []error) {
	attr, errs := att.attractionRepo.StoreAttraction(attract)

	if len(errs) > 0 {
		return nil, errs
	}
	return attr, nil
}

func (att *AttractionService) Attraction(id uint) (*entity.Attraction, []error) {
	attr, errs := att.attractionRepo.Attraction(id)

	if len(errs) > 0 {
		return nil, errs
	}
	return attr, errs
}

func (att *AttractionService) UpdateAttraction(attract *entity.Attraction) (*entity.Attraction, []error) {
	attr, errs := att.attractionRepo.UpdateAttraction(attract)

	if len(errs) > 0 {
		return nil, errs
	}
	return attr, nil
}

func (att *AttractionService) DeleteAttraction(id uint) (*entity.Attraction, []error) {
	attr, errs := att.attractionRepo.DeleteAttraction(id)
	if len(errs) > 0 {
		return nil, errs
	}
	return attr, nil
}
