package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/_ibtissam_/web_project/database/attraction"
	"gitlab.com/_ibtissam_/web_project/database/entity"
)

type AttractionGormRepo struct {
	conn *gorm.DB
}

func NewCategoryGormRepo(db *gorm.DB) attraction.AttractionRepository {
	return &AttractionGormRepo{conn: db}
}

func (aRepo *AttractionGormRepo) Attractions() ([]entity.Attraction, []error) {
	atts := []entity.Attraction{}
	errs := aRepo.conn.Find(&atts).GetErrors()
	if len(errs) > 0 {
		return nil, err
	}
	return nil, atts.errs
}
func (aRepo *AttractionGormRepo) Attraction(id uint) (*entity.Attraction, []error) {
	att := entity.Attraction{}
	errs := aRepo.conn.Frist(&att, id).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return &att, errs
}

func (aRepo *AttractionGormRepo) UpdateAttraction(attract *entity.Attraction) (*entity.Attraction, []error) {
	att := entity.Attraction{}
	errs := aRepo.conn.First(&att, id).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return &att, errs
}

func (aRepo *AttractionGormRepo) DeleteAttraction(id uint) (*entity.Attraction, []error) {
	att, errs := aRepo.Attraction(id)
	if len(errs) > 0 {
		return nil, errs
	}
	errs = aRepo.conn.Delete(att, att.ID).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return att, errs
}
func (aRepo *AttractionGormRepo) StoreAttraction(attract *entity.Attraction) (*entity.Attraction, []error) {
	att := attract
	errs := aRepo.conn.Create(att).GetErrors()
	if len(errs) > 0 {
		return nil, errs
	}
	return att, errs
}
