package attraction

import "gitlab.com/_ibtissam_/web_project/database/entity"

type AttractionRepository interface {
	attractions() ([]entity.Attraction, []error)
	Attraction(id uint) (*entity.Attraction, []error)
	UpdateAttraction(attraction *entity.Attraction) (*entity.Attraction, []error)
	DeleteAttraction(id uint) (*entity.Attraction, []error)
	StoreAttraction(attraction *entity.Attraction) (*entity.AttractionCategory, []error)
}
